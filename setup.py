"""HC Google Scrape setup."""
from setuptools import setup, find_packages

requirements_path = 'requirements.txt'
with open(requirements_path) as f:
    requirements = f.read().splitlines()

    # Account for custom packages.
    _requirements = []
    dep_links = []
    multi_line = False
    for ix, req in enumerate(requirements):
        if '--extra-index' in req:
            dep_links.append(
                req.
                replace(
                    '--extra-index-url ',
                    ''
                    )
                )
        else:
            _requirements.append(req)

    requirements = _requirements

setup(name='scrape_google',
      version='1.0.4',
      description='Simplifies operations around webscraping.',
      url='https://gitlab.com/1hc_public/scrape_google',
      author='1 Howard Capital LLC',
      author_email='support@1howardcapital.com',
      license="Proprietary",
      classifiers=[
          'License :: Other/Proprietary License',
          ],
      packages=find_packages('.',
                             exclude=['tests',
                                      'tests.*',
                                      '*.tests',
                                      '*.tests.*']),
      install_requires=requirements,
      dependency_links=dep_links,
      zip_safe=False)
