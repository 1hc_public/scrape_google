"""Google scrape tests."""
import unittest
import os

from selenium import webdriver

from hc_manager.scrape_google import scrape_target


DOCKER_ENV = True if os.system('sudo pwd') != 0 else False

if DOCKER_ENV:
    tgt = scrape_target.Target(
        path_driver='http://selenium__standalone-chrome:4444/wd/hub',
        update_driver=True
        )
else:
    tgt = scrape_target.Target(update_driver=True)


class TestLoadGoogle(unittest.TestCase):
    """Test Google loading."""

    def test_load_google(self):
        """Test if Google loads."""
        tgt.load_google()
        self.assertEqual(
            tgt.driver.current_url,
            'https://www.google.com/'
            )


class TestGetInput(unittest.TestCase):
    """Test if we are getting search input form."""

    def test_get_search_input(self):
        """Test if we got web element."""
        tgt.get_search_input()
        self.assertIsInstance(
            tgt.form,
            webdriver.remote.webelement.WebElement
            )


class TestSearch(unittest.TestCase):
    """Test actual search functionality."""

    def test_search_text(self):
        """Test if actually searched."""
        tgt.search_text()
        current_url = tgt.driver.current_url
        self.assertIn(
            'google.com/search',
            current_url
            )


class TestGetSummaries(unittest.TestCase):
    """Test if we are able to collect page corpus."""

    def test_get_page_summaries(self):
        """Test corpus is list."""
        tgt.get_page_summaries()
        self.assertIsInstance(
            tgt.corpus,
            list
            )


class TestCleanseSummaries(unittest.TestCase):
    """Test if cleansing properly."""

    def test_cleanse_page_summaries(self):
        """Test if corpus populated."""
        tgt.cleanse_page_summaries()
        self.assertEqual(
            tgt.corpus.columns.tolist(),
            ['google_index', 'corpus', 'url']
            )


class TestCloseDriver(unittest.TestCase):
    """Close out."""

    def test_close(self):
        """Close webdriver."""
        self.assertIsNone(
            tgt.driver.close()
            )

# TODO: Test data write.
